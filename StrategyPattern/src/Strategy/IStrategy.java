package Strategy;

public interface IStrategy {
	public void sort();
}
