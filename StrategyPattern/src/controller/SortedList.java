package controller;

import Strategy.IStrategy;

public class SortedList {
	private IStrategy strategy;

	public IStrategy getStrategy() {
		return strategy;
	}

	public void setStrategy(IStrategy strategy) {
		this.strategy = strategy;
	}

	public void sort() {
		this.strategy.sort();
	}
}
