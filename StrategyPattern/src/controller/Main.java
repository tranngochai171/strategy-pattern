package controller;

import Strategy.HeapSort;
import Strategy.QuickSort;
import Strategy.SelectionSort;

public class Main {
	public static void main(String[] args) {
		SortedList sortedList = new SortedList();
		sortedList.setStrategy(new HeapSort());
		sortedList.sort();
		sortedList.setStrategy(new QuickSort());
		sortedList.sort();
		sortedList.setStrategy(new SelectionSort());
		sortedList.sort();
	}
	
}
